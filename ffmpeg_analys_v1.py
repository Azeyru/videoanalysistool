import os
import os.path
import subprocess
import glob
from io import StringIO

from config import trimmed_video, analysis_video_folder, current_path

def cmd_analysis(trim_file, file_name, folder_name):
	
	
	videos = (current_path + '\\' +trim_file).replace('\\','/')[2:]
	
	output_video_folder = os.path.join(analysis_video_folder, folder_name)	
	if not os.path.exists(output_video_folder):
		os.makedirs(output_video_folder)
		print("Folder Created at:" + output_video_folder)
		
	else:
		print("Folder " +file_name+ " is already there")
		
	print("FFMPEG Started...")
	try:
		cmd_analys = [
			'ffmpeg', '-f', 
			'lavfi', '-i',
			'amovie='+videos+',asplit=3[sv][eb][av];[sv]showvolume=b=4:w=720:h=68[sv-v];[eb]ebur128=video=1:size=720x540:meter=18[eb-v][out1];[av]avectorscope=s=720x540:draw=line:zoom=1.3:rc=40:gc=160:bc=80:rf=1:gf=8:bf=7[av-v];[sv-v][eb-v][av-v]vstack=3[1c];movie='+videos+',split=4[v][wf][wfc][vs];[wf]waveform=m=1:d=0:r=0:c=7[wf-vus];[wf-vus][v]scale2ref=iw:1148-ih[wf-va][sig];[wf-va]setsar=1[wf-v];[wfc]waveform=m=0:d=0:r=0:c=7,scale=610x684,setsar=1[wfc-v];[vs]vectorscope=m=color3:g=color,scale=610x464,setsar=1[vs-v];[sig][wf-v]vstack[2c];[wfc-v][vs-v]vstack[3c];[1c][2c][3c]hstack=3,scale=1280:-1[out0]', 
			'-async', '1', output_video_folder+'\\'+file_name+'.mp4'
			]
		
		analysis_process = subprocess.Popen(cmd_analys, stdout = subprocess.PIPE, stderr = subprocess.STDOUT, universal_newlines = True)
		analysis_process_data = str(analysis_process.communicate())
		print("FFMEPG process finished succesfully.....")
		
	except Exception as exception:
		print('FFMEPG threw: ' + str(exception))
		
	finally:
		print("Exiting FFMEPG...")
					
def main():
	trimmed_root_folder = os.listdir(trimmed_video)
	list = []
	
	for trimmed_folders in trimmed_root_folder:
		list.append(trimmed_folders)
		
	for trimmed_video_folders in list:
		trimmed_video_list = glob.glob(os.path.join(trimmed_video, trimmed_video_folders))

		for videos in trimmed_video_list:
			trim_files = glob.glob(os.path.join(videos, '*'))
			
			for trim_file in trim_files:
				
				file_name = trim_file.split("\\")[2].split(".")[0]
				extension = trim_file.split('\\')[2].split(".")[1]
				print("Loaded file: " + file_name + " | Type: " + extension)
				cmd_analysis(trim_file, file_name, trimmed_video_folders)
				print("Task finished...")
		
if __name__ == '__main__':
	main()