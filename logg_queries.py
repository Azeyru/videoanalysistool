import pymysql
import pymysql.cursors
import datetime
class QUERIES:
	#defining Variables constructor
	def __init__(self):
		self.host_name = "localhost"
		self.user_name = "root"
		self.password = "root"
		self.db_name = "videologs"

	#opening Connection
	def connections(self):
		print("Opening Connection")
		connection1 = pymysql.connect(
									host = self.host_name,
									user = self.user_name,
									password = self.password,
									db = self.db_name,
									charset='utf8mb4',
									cursorclass=pymysql.cursors.DictCursor
									)
		return connection1
	#Filewatch 1-insert info to DB
	def insert_fw1(self, EntryTime, Status, currentDT):
		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			idi = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
			print(idi)
			self.cursor.execute('INSERT INTO logss(item_id, entry_date, overall_job_status, overall_job_update_time) VALUES('+"'"+str(idi)+"'"+','+"'"+str(EntryTime)+"'"+','+"'"+str(Status)+"'"+','+"'"+str(currentDT)+"'"')')
			connection1.commit()
			print("Logg Table Updated with Filewatcher 1 Info......")
		except:
			print("Failed: Rollbacking...")
			connection1.rollback()
			
		finally:
			print("Closing connection...")
			connection1.close()

	#Filewatch 1-insert info to DB
	def insert_info_ext(self, idi, overallstatus, log_name, videos, file_size, extension, entry_time):

		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			self.cursor.execute('INSERT INTO logss(item_id, overall_job_status, filename, filepath, filesize, file_extension, entry_date) VALUES('
				+"'"+str(idi)+"'"+','
				+"'"+str(overallstatus)+"'"+','
				+"'"+str(log_name)+"'"+','
				+"'"+str(videos)+"'"+','
				+"'"+str(file_size)+"'"+','
				+"'"+str(extension)+"'"+','
				+"'"+str(entry_time)+"'"')')	
			connection1.commit()
			print("Logg Table Updated with Filewatcher 1 Info......")
		except:
			print("Failed: Rollbacking...")
			connection1.rollback()
			
		finally:
			print("Closing connection...")
			connection1.close()

	#Update DB	
	def updateinfo(self, idi, status, timetaken, duration, v_codec, a_codec, sample_rate, a_channel):
		#TABLE: item_id, overall_job_status, overall_job_update_time, fileduration, video_codec_name, Audio_codec_name, Audio_sample_rate, Audio_channel_layout
		try:
			q = QUERIES()
			connection1 = q.connections()
			self.cursor = connection1.cursor()
			self.cursor.execute('UPDATE logss SET overall_job_status='+str(status)+', overall_job_update_time='str(timetaken)', fileduration='str(duration)', video_codec_name='str(v_codec)', Audio_codec_name='str(a_codec)', Audio_sample_rate='str(sample_rate)', Audio_channel_layout='str(a_channel)' WHERE item_id='str(idi)'')
			
			#self.cursor.execute("UPDATE loggs SET overall_job_status=%s, overall_job_update_time=%s, fileduration=%s, video_codec_name=%s, Audio_codec_name=%s, Audio_sample_rate=%s, Audio_channel_layout=%s WHERE item_id='str(idi)' " % (str(status), str(timetaken), str(duration), str(v_codec), str(a_codec), str(sample_rate), str(a_channel))
			connection1.commit()
			print("Table loggs got Updated")
		except:
			print("Failed to update loggs: Rollbacking...")
			connection1.rollback()
			
		finally:
			print("Closing connection...")
			connection1.close()
