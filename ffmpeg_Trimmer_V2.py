import os
import os.path
import logging
import subprocess
import itertools as tool
from io import StringIO

from config import log_source, current_path, path_to_watch, time_config, trimmed_video_folder

print("Trim initiated...")

print("The time taken will dependent on various factors of input file")

def get_video_details(video, start_time, end_time):

	buffer_start_time = str(float(start_time) - float(time_config))
	end_tm = str(float(end_time) + float(time_config))
	duration = str(float(end_tm) - float(buffer_start_time))
	
	output_video_folder = os.path.join(trimmed_video_folder, video)
	if not os.path.exists(output_video_folder):
		os.makedirs(output_video_folder)
		print("Folder Created at:" + output_video_folder)
		
	return buffer_start_time, duration, output_video_folder

def video_trimmer(video_name_path, video, start_time, end_time, count):
	
	buffer_start_time, duration, output_video_folder = get_video_details(video, start_time, end_time)

	print("FFMPEG Initialized...")

	try:
		cmd_trim_video = [
				'ffmpeg', 
				'-ss', buffer_start_time, 
				'-i', video_name_path, 
				'-t', duration, 
				'-c:v', 'libx264', '-s', '1280x720', 
				'-c:a', 'aac', 
				'-strict', 'experimental', 
				'-b:a', '128k', 
				'-avoid_negative_ts', '1', output_video_folder+'\\'+video+str(count)+'.mp4'
				]
		trim_process = subprocess.Popen(
				cmd_trim_video, 
				stdout = subprocess.PIPE, 
				stderr = subprocess.STDOUT)
		cmd_trim_video_data = str(trim_process.communicate())
		print("ffmpeg finished processing the Video")
		
	except Exception as exception:
		print('FFMPEG Threw error at: '+ str(exception))
		
	finally:
		print("Exiting ffmpeg")
		
	return cmd_trim_video_data

def read_file_data(file, temp, count):
	
	video = file.split('.')[0] # contains video name
	video_name = file.split('.log')[0] # contains video name.ext
	video_name_path = current_path + '\\' + path_to_watch + '\\' +video_name #location to input videos
	
	with open(log_source+'/'+file,'r') as data:
		
		for start, end in tool.zip_longest(*[data]*2):
			if temp != file:
				count = 0
			count = count + 1

			if end:
				start_time = start.split('=')[1]
				end_time = end.split('=')[1]
				print(file + ": " + "start time: " + start_time,"end time:" + end_time)

				try:
					video_trimmer(video_name_path, video, start_time, end_time, count)
					pass
				except Exception as err:
					print(err)
					
				finally:
					print('Process Completed....')
					pass
					
			temp = file	
def main():
	for files in log_source:
		log_file_list =  os.listdir(log_source) #contains list of all video files
	
	temp = log_file_list[0]
	count = 0
	
	for file in log_file_list:
		video = file.split('.')[0]
		read_file_data(file, temp, count)
		
if __name__ == '__main__':
	main()