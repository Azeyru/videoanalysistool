import glob
import os
import os.path
import subprocess
import logging
from io import StringIO

from config import video_source, log_destination, current_path_movie_input

from log_file import load_logger

logger = []

console = logging.getLogger()

print("FFMPEG Started")
	
def cmd_black(videos):
	videos = videos.replace('\\','/')
	
	try:
		cmd_black = [
				'ffprobe', 
				'-f', 'lavfi', 
				'-i', 'movie='+ current_path_movie_input +'/' + videos +',blackdetect[out0]', 
				'-show_entries', 
				'tags=lavfi.black_start,lavfi.black_end', 
				'-of', 'default=nw=1', '-v', 'quiet',
				]
		black_process = subprocess.Popen(
			cmd_black, 
			stdout = subprocess.PIPE, 
			stderr = subprocess.STDOUT,
			universal_newlines=True
		)
		for line in (black_process.stdout):
			logger.append(line.strip())
			
	except black_process.CalledProcessError as exception:
		print('Exception occured: ' + str(exception))
		
	else:
		print('Subprocess Finished, Exiting ffmpeg')
		
	return logger

def main():
	for files in video_source:
		video_list = glob.glob(os.path.join(files, '*'))
		
		for videos in video_list:
		
			log_name = videos.split("\\")[1].split(".")[0]
			extension = videos.split('\\')[1].split(".")[1]
			print("Loaded file: " + log_name + " | Type: " + extension)
			
			console, handler = load_logger(log_name, extension)

			print("Detecting Black Frames \n")
			black_data = cmd_black(videos)
			
			remove_duplicate = []
			for data in black_data:
				if data not in remove_duplicate:
					remove_duplicate.append(data)
				
			for data in remove_duplicate:
				console.info(data)
				print(data)
			console.removeHandler(handler)
			logger[:] = []
			remove_duplicate[:] = []
			
			print(" Data has been successfully logged")

			
if __name__ == '__main__':
	main()			
	