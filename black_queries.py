import PyMySQL
from settings.py import connection as con

class QUERIES:
	#defining Variables constructor
	def __init__(self):
		self.host_name = "localhost"
		self.user_name = "root"
		self.password = "root"
		self.db_name = "videologs"

	#opening Connection
	def connection(self):
		self.connection = pymysql.connect(
									host = self.host_name,
									user = self.user_name,
									password = self.password,
									db = self.db_name,
									charset='utf8mb4',
									cursorclass=pymysql.cursors.DictCursor
									)
		self.cursor = self.connection.cursor()
	#insert to DB
	def insert(self, insert_data):
		try:
			with con.cursor as cursor:
				cursor.execute(insert_data)
			con.commit()
		except:
			con.rollback()
			
		finally:
			con.close()
	#Update DB	
	def update(self, update_date):
		try:
			with con.cursor as cursor:
				cursor.execute(update_date)
			con.commit()
		except:
			con.rollback()
			
		finally:
			con.close()