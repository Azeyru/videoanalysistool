import os
import time
import logging
import datetime 

from config import path_to_watch

class WATCH:
	#defining Variables constructor
	def __init__(self):
		self.initial = dict([(files, None) for files in os.listdir (path_to_watch)])
		self.current_path = os.getcwd()
		
		logger_format = '%(levelname)s: %(asctime)s - %(message)s'
		logging.basicConfig(format = logger_format, filename="FileWatcher.log", level = logging.NOTSET)
		self.console = logging.getLogger()

		self.EntryTime = datetime.datetime.now()
		self.Status = "In Progress/FileWatcher-1"

	def watcher(self):
		print("Video watcher is now initialized")
		
		while True:
			try:
				time.sleep (1)
				check = dict([(files, None) for files in os.listdir (path_to_watch)])
				added = [files for files in check if not files in self.initial]

				for files in added:
					if files.split(".")[1] == 'txt':
						if added:
							
							#Enable to Add logs from FW to the DB
							'''self.currentDT = datetime.datetime.now()
							from logg_queries import QUERIES
							loggQ = QUERIES()
							loggQ.insert_fw1(self.EntryTime, self.Status, self.currentDT)
							print("Success...........1")'''

							added_string = "File Added: ", ", ".join(added)
							self.console.info(str(added_string))
							print(str(added_string))

							os.system('python ffmpeg_video_info_extractor_V4.py')
							os.system('python ffmpeg_black_extractor_V4.py')

						self.initial = check
					else:
						print("Removing the file"+files)
						os.remove(os.path.join(self.current_path,'video_',files))
						print("removed...")

			except Exception as exception:
					print('Exception occured: ' + str(exception))
			finally:
				print('Waiting for input...')
				
def main():
	try:
		watch = WATCH()
		watch.watcher()
	except:
		print("Exiting...")
				
if __name__ == '__main__':
	main()
		