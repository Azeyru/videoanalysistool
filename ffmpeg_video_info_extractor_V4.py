import glob
import os
import os.path
import subprocess
import logging
from io import StringIO
import datetime, time
import json

from config import video_source, log_destination, current_path
from log_file import load_logger_info

class INFOEXT:
	#constructor
	def __init__(self):
		self.log = []
		self.console = logging.getLogger()	
		self.data = ""
		#self.entry_time = datetime.datetime.now()
		#self.idi = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	
	#FFMPEG processor, returns the extracted Metadata
	def cmd_stream(self, videos):
		print("FFMPEG STARTED...")
		cmd_stream = [
				'ffprobe', 
				'-i', videos, '-hide_banner',
				'-print_format','json', 
				#'ffmetadata'
				'-show_format',
				'-show_streams' 
				]
		print("FFMPEG Processing...")
		process_stream = subprocess.Popen(
				cmd_stream, 
				stdout = subprocess.PIPE, 
				stderr = subprocess.STDOUT,
				universal_newlines=True
				)
		for data in (process_stream.stdout):
			self.log.append(data.strip())
		#cmd_stream_data = str(process_stream.communicate())
		return self.log

	#gets the full path for all the media files
	def fullPaths(self, directory):
		datalist = []
		for dirpath,_,filenames in os.walk(directory):
			for files in filenames:
				datalist.append(os.path.abspath(os.path.join(dirpath, files)))
		return datalist

	#returns the list of all the video files from the specified text files
	def read_data(self, text_data):
		print("collecting list of all the media files")
		pathlist = []
		for txts in text_data:
			with open(txts, 'r') as pathdata:
				for data in pathdata.readlines():
					if data  is not '\n':
						pathlist.append(str(data).strip())
		#Holds absolutepath for all the video
		video_path_list = []
		for files in pathlist:
			txt = INFOEXT()
			video_path_list.append(txt.fullPaths(files))

		return [item for sublist in video_path_list for item in sublist]

	def insertTodb(self, idi, overallstatus, log_name, videos, file_size, extension, entry_time):
		#load the class from db.py
		from logg_queries import QUERIES
		loggQ = QUERIES()
		loggQ.insert_info_ext(idi, overallstatus, log_name, videos, file_size, extension, entry_time)

	def updateTodb(self, idi, status, timetaken, duration, v_codec, a_codec, sample_rate, a_channel):
		from logg_queries import QUERIES
		loggQ = QUERIES()
		loggQ.updateinfo(idi, status, timetaken, duration, v_codec, a_codec, sample_rate, a_channel)

	def getmeta(self, stream_data):
		index = ''
		indexflag = ''
		duration = ''
		v_codec = ''
		a_codec = ''
		sample_rate = ''
		a_channel = ''

		dictt = []
		for dic in stream_data:
			dic = dic[:-1]
			if dic is not '':
				dictt.append(dic)
		for lines in  range(len(dictt)):
			str_data = dictt[lines]

			#collecting Index number, to get codecs
			if str_data.split(":")[0] == '"index"':
				indexflag = str_data.split(":")[1]

			#collecting video codec based on index 
			if str_data.split(":")[0] == '"codec_name"' and indexflag == ' 0':
				v_codec = str_data.split(":")[1]

			#collecting video codec based on index 
			if str_data.split(":")[0] == '"codec_name"' and indexflag == ' 1':
				a_codec = str_data.split(":")[1]
				
			#collecting Durtation of video
			if str_data.split(":")[0] == '"duration"':
				duration = str_data.split(":")[1]
			#collecting audio sample rate  of video
			if str_data.split(":")[0] == '"sample_rate"':
				sample_rate = str_data.split(":")[1]
			#collecting Audio Channels of video
			if str_data.split(":")[0] == '"channels"':
				a_channel = str_data.split(":")[1]
		return duration, v_codec, a_codec, sample_rate, a_channel

	#main method
	def main(self):
		print("Getting Paths from the text file")
		txt_list = ''
		for files in video_source:
			txt_list = glob.glob(os.path.join(files, '*'))
		ift = INFOEXT()
		videos_path = ift.read_data(txt_list)

		for videos in  videos_path:
			entry_time = datetime.datetime.now()
			idi = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
			overallstatus = "In Progress/InfoExtractor" 

			print(videos)
			filename = os.path.basename(videos)
			log_name = filename.split(".")[0]
			extension = filename.split(".")[1]
			print("Loaded file: " + log_name + " | Type: " + extension)

			file_size = round(os.path.getsize(videos)/(1024*1024),5)

			console, handler = load_logger_info(log_name, extension)
			arrange = []
			print("Extracting Media Details ")

			inf = INFOEXT()
			#inserting into DB
			inf.insertTodb(idi, overallstatus, log_name, videos, file_size, extension, entry_time)

			stream_data = inf.cmd_stream(videos)
			arrange.append(stream_data)

			duration, v_codec, a_codec, sample_rate, a_channel = ift.getmeta(stream_data)
			timetaken = entry_time - datetime.datetime.now()
			
			#Updating DB
			inf.updateTodb(idi, overallstatus, timetaken, duration, v_codec, a_codec, sample_rate, a_channel)

			for data in arrange:
				console.info(stream_data)

			arrange[:] = []
			self.log[:] = []
			console.removeHandler(handler)
			#delay
			time.sleep(1)

if __name__ == '__main__':
	inf = INFOEXT()
	inf.main()			
